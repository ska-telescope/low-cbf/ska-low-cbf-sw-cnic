Low CBF CNIC Software
=====================

`The CNIC Firmware Documentation <https://developer.skao.int/projects/ska-low-cbf-fw-cnic/en/latest>`_
may be useful to cross-reference (e.g. it contains information on SPS SPEAD packet structure).

.. toctree::
   :maxdepth: 2
   :caption: Low CBF CNIC Software Documentation:

   README

.. toctree::
   :maxdepth: 6
   :caption: CNIC API

   modules

.. toctree::
   :maxdepth: 2
   :caption: ITango notes

   itango_notes


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
