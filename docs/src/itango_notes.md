# Notes on ITango Usage

Here we present a basic usage of FPGA methods from `ITango` shell.

## Device Setup
Once `ITango` shell is open (e.g. from `k9s` console) we need to create a proxy device:

```
In [1]: dev = Device("low-cbf/processor/0.0.0")
```

Next, we'll download a personality from the registry -  either CAR (Nexus) for released personality versions - or GitLab for development versions. However as this download can take longer than the default `ITango` command timeout, we first extend the timeout value:

```
In [2]: dev.set_timeout_millis( 12_000 )

In [3]: dev.SelectPersonality('{ "personality": "cnic", "version":"0.1.2-dev.a9cbe", "source": "gitlab"}')
```

We bring the device online with:

```
In [4]: dev.adminMode=0

In [5]: dev.adminMode
Out[5]: <adminMode.ONLINE: 0>
```

## Calling Methods
In general there are two kinds of methods we can call from `ITango` shell:

 1. top level methods on the device itself
 2. the methods on one of constituent 'peripherals'
 
 As Tango commands could only accept a single argument, we need to pack into it all the details of the called function - the function name, all function arguments (if any) and, optionally, the 'peripheral' name if applicable. All this information is stored in a JSON string with the structure along these lines:
 
    { 
      "peripheral" : "peripheral_name", 
      "method":    "method_name", 
      "arguments": 
      { 
        "arg1_name": value1,
        "arg2_name": value2,
        ...
      }
    }
 
So in order to call `dump_cap()` method on `hbm_pktcontroller` peripheral, in `ITango` shell we would type:

```
In [6]: dev.CallMethod('{ "peripheral" : "hbm_pktcontroller", "method": "dump_pcap", "arguments": { "out_filename" : "zulu.pcap", "packet_size": 128}}')
Out[6]: [array([0], dtype=int32), ['OK']]
```

or to call the top level method (i.e. not bound to a peripheral) `transmit_pcap()`

```
In [7]: dev.CallMethod('{ "method": "transmit_pcap", "arguments": { "in_filename" : "spead-5a.pcap"}}')
Out[7]: [array([0], dtype=int32), ['OK']]
```

## A More Realistic Scenario
The previous section was mostly focused on illustrating how to convert the function names and arguments into JSON format accepted by ``Tango`` infrastructure.

Here we present a possibly more useful example where in one ``ITango`` shell we start a packet receiver and in the other we initiate transmission of data packets stored in  [``pcap``](https://en.wikipedia.org/wiki/Pcap) format.

The reciever
````
dev = DeviceProxy("low-cbf/processor/0.0.0")
dev.CallMethod(‘{“method”: “receive_pcap”, “arguments”: {“out_filename”: "/test-data/outputfile.pcap"}}’)
while not dev.finished_receive:
    sleep(5)
print("Finished receiving pcap!")
````

A separate transmitter ``ITango`` shell:
````
dev = DeviceProxy("low-cbf/processor/0.0.0")
dev.CallMethod(‘{“method”: “transmit_pcap”, “arguments”: {“in_filename”: "/test-data/inputfile.pcap", "rate": 25}}’)
````
