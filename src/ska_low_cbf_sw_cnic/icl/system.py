# -*- coding: utf-8 -*-
#
# Copyright (c) 2024 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""
CNIC "System" peripheral abstraction.
"""
from ska_low_cbf_fpga import DISCOVER_ALL, FpgaPeripheral, IclField


def mask_shift(value: int, mask: int) -> int:
    """
    Apply bit mask and down shift so result counts from zero.

    :param value: raw value to apply mask/shift to
    :param mask: bit mask - all binary 1s should be consecutive
      (e.g. ``0b1111_0000`` is good, ``0b0101_0101`` is bad)
    """
    # shift by mask's MSB position (counting from one) minus number of bits set
    return (value & mask) >> (mask.bit_length() - mask.bit_count())


class System(FpgaPeripheral):
    _user_attributes = {DISCOVER_ALL}

    CAP_HBM_SIMPLEX_TX = 0b1111
    CAP_HBM_SIMPLEX_RX = 0b1111 << 4
    CAP_HBM_DUPLEX_TX = 0b1111 << 8
    CAP_HBM_DUPLEX_RX = 0b1111 << 12
    CAP_ETH_A = 1 << 16
    CAP_ETH_B = 1 << 17
    CAP_VD_1 = 1 << 18
    CAP_VD_2 = 1 << 19
    CAP_ETH_A_LOOPBACK = 1 << 22

    @property
    def ptp_a_capable(self) -> IclField[bool]:
        """PTP on port A capable?"""
        return IclField(
            description="PTP port A capability",
            type_=bool,
            value=bool(self.firmware_capability & self.CAP_ETH_A),
        )

    @property
    def ptp_b_capable(self) -> IclField[bool]:
        """PTP on port B capable?"""
        return IclField(
            description="PTP port B capability",
            type_=bool,
            value=bool(self.firmware_capability & self.CAP_ETH_B),
        )

    @property
    def datagen_1_capable(self) -> IclField[bool]:
        """Is the vd_datagen peripheral operational?"""
        return IclField(
            description="vd_datagen capability",
            type_=bool,
            value=bool(self.firmware_capability & self.CAP_VD_1),
        )

    @property
    def datagen_2_capable(self) -> IclField[bool]:
        """Is the vd_datagen_2 peripheral operational?"""
        return IclField(
            description="vd_datagen_2 capability",
            type_=bool,
            value=bool(self.firmware_capability & self.CAP_VD_2),
        )

    @property
    def eth_a_loopback_capable(self) -> IclField[bool]:
        """Loopback on Ethernet port A capable?"""
        return IclField(
            description="Ethernet A Loopback capability",
            type_=bool,
            value=bool(self.firmware_capability & self.CAP_ETH_A_LOOPBACK),
        )

    @property
    def hbm_tx_duplex_buffers(self) -> IclField[int]:
        """Number of HBM transmit buffers available in duplex mode."""
        return IclField(
            description="HBM transmit buffers available in duplex mode",
            type_=int,
            value=mask_shift(self.firmware_capability, self.CAP_HBM_DUPLEX_TX),
        )

    @property
    def hbm_tx_simplex_buffers(self) -> IclField[int]:
        """Number of HBM transmit buffers available in simplex mode."""
        return IclField(
            description="HBM transmit buffers available in simplex mode",
            type_=int,
            value=mask_shift(self.firmware_capability, self.CAP_HBM_SIMPLEX_TX),
        )

    @property
    def hbm_rx_duplex_buffers(self) -> IclField[int]:
        """Number of HBM receive buffers available in duplex mode."""
        return IclField(
            description="HBM receive buffers available in duplex mode",
            type_=int,
            value=mask_shift(self.firmware_capability, self.CAP_HBM_DUPLEX_RX),
        )

    @property
    def hbm_rx_simplex_buffers(self) -> IclField[int]:
        """Number of HBM receive buffers available in simplex mode."""
        return IclField(
            description="HBM receive buffers available in simplex mode",
            type_=int,
            value=mask_shift(self.firmware_capability, self.CAP_HBM_SIMPLEX_RX),
        )
