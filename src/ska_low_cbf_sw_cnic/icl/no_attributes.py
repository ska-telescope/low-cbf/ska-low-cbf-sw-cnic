# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more information.

from ska_low_cbf_fpga import FpgaPeripheral


class NoAttributes(FpgaPeripheral):
    """
    FpgaPeripheral that specifies _user_attributes = None,
    for peripherals that we do not want to monitor in the control system
    """

    _user_attributes = None
