# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""
Virtual Digitiser - SPS SPEAD Packet Generator
ICL (abstraction)
"""
from ipaddress import ip_address

from ska_low_cbf_fpga import DISCOVER_PROPERTIES, FpgaPeripheral, IclField


def _split_mac(mac: str) -> (int, int):
    """
    Split a colon-seperated MAC address string into two 32-bit numbers

    :returns: High two bytes, Low four bytes
    """
    mac_int = int("".join(mac.split(":")), 16)
    return (mac_int >> 32), (mac_int & 0xFFFF_FFFF)


def _join_mac(high: int, low: int) -> str:
    """
    Convert two 32-bit numbers into a MAC address string

    :param high: High two bytes
    :param low: Low four bytes
    :return: colon-seperated string, e.g. 'AB:CD:EF:01:02:03'
    """
    mac_int = (high << 32) | low
    return ":".join(f"{mac_int:012x}"[i : i + 2] for i in range(0, 12, 2)).upper()


class SpeadSPS(FpgaPeripheral):
    _user_attributes = {
        DISCOVER_PROPERTIES,
        "udp_src_port",
        "udp_dst_port",
        "enable_packetiser",
        "no_of_packets_sent",
    }

    @property
    def ethernet_destination(self) -> IclField[str]:
        return IclField(
            description="Ethernet Destination MAC Address",
            value=_join_mac(
                self.ethernet_dst_mac_u.value, self.ethernet_dst_mac_l.value
            ),
            type_=str,
        )

    @ethernet_destination.setter
    def ethernet_destination(self, mac_address: str):
        self.ethernet_dst_mac_u, self.ethernet_dst_mac_l = _split_mac(mac_address)

    @property
    def ethernet_source(self) -> IclField[str]:
        return IclField(
            description="Ethernet Source MAC Address",
            value=_join_mac(
                self.ethernet_src_mac_u.value, self.ethernet_src_mac_l.value
            ),
            type_=str,
        )

    @ethernet_source.setter
    def ethernet_source(self, mac_address: str):
        self.ethernet_src_mac_u, self.ethernet_src_mac_l = _split_mac(mac_address)

    @property
    def ipv4_source(self) -> IclField[str]:
        return IclField(
            description="IP v4 Source Address",
            value=str(ip_address(self.ipv4_src_addr.value)),
            type_=str,
        )

    @ipv4_source.setter
    def ipv4_source(self, address: str):
        self.ipv4_src_addr = int(ip_address(address))

    @property
    def ipv4_destination(self) -> IclField[str]:
        return IclField(
            description="IP v4 Destination Address",
            value=str(ip_address(self.ipv4_dst_addr.value)),
            type_=str,
        )

    @ipv4_destination.setter
    def ipv4_destination(self, address: str):
        self.ipv4_dst_addr = int(ip_address(address))
