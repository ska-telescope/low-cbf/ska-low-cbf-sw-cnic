# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Configuration / coordination between virtual_digitiser and vd_datagen"""
import typing
from dataclasses import dataclass

from ska_low_cbf_sw_cnic.icl.vd_datagen import SOURCES_PER_STREAM, Source
from ska_low_cbf_sw_cnic.icl.virtual_digitiser import VDChannelConfig

HZ_PER_CHANNEL = 781_250


def frequency_from_id(freq_id: int) -> int:
    """Get the frequency (Hz) of a given freq_id."""
    assert freq_id >= 0, "Frequency ID must be positive"
    return freq_id * HZ_PER_CHANNEL


def id_from_frequency(frequency: int) -> int:
    """Get the freq_id of a given frequency (Hz)."""
    # TODO what to do if not valid?
    return frequency // HZ_PER_CHANNEL


@dataclass
class StreamConfig:
    spead_stream: VDChannelConfig
    sources: typing.List[Source]

    def __post_init__(self):
        if len(self.sources) > SOURCES_PER_STREAM:
            raise ValueError("Too many sources!")
