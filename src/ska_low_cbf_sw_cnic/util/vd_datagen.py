# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

"""
Standalone code to create configuration for the vd_datagen module
in the CNIC firmware.
vd_datagen = virtual digitiser data generation
---------------------------------------------
Introduction:
The vd_datagen module writes SPS-like sample data into the HBM in the CNIC.
It is configured via a 1 Mbyte block of memory that is written to via the
CNIC register interface.
----------------------------------------------
Configuration memory:
   The memory is organised into blocks of 64 bytes.
   Each block of 64 bytes specifies one polarisation, for a single source in the sky
   In total there is 1 MByte of configuration memory:
     Bytes 0 to 63  : Source 1 for data stream 1, pol 0
     (see read side below for specification of contents)
     Bytes 0 to 255 : All 4 sources for data stream 1, pol 0
     Bytes 0 to 511 : All 8 sources for data stream 1, 2 polarisations
     Bytes 512-1023 : Data stream 2
     ...
     Bytes 524264-524287 : Data stream 1024
     Bytes 524288-1048576 : Second buffer, identical to first buffer,
     to enable polynomial updates.
   Within each 64 byte block:
     bytes 0-7 = c0,
     ...
     bytes 40-47 = c5,
       c0 to c5 are double precision floating point values for the delay polynomial:
       c0 + c1*t + c2 * t^2 + c3 * t^3 + c4 * t^4 + c5 * t^5
       Units for c0,.. c5 are ns/s^k for k=0,1,..5
     bytes 48-55 = Sky frequency in GHz
       Used to convert the delay (in ns) to a phase rotation.
     bytes 56-63 = Data source specification
       bits 14:0  = Random number generator seed.
                       -- OR --
         phase step for generation of sinusoids.
         used as the step into a 32768 deep (sin,cos) lookup table
         So a step of 0 = DC = center of channel
         step of +/-1 = (1/1080ns)*(1/32768) = +/-28.257 Hz
         step of 8 = (1/1080ns)*(8/32768) = 226.056 Hz (= low CBF correlator filterbank resolution)
         step of 4*24=96 = 5.4kHz (= standard integration bandwidth for the low CBF correlator)
         maximum step is +/-16383 = +/-462 kHz --> outside the used bandwidth due to 32/27 oversampling.
       bit 15     = '0' to select pseudo-random numbers, '1' to select sinusoid
       bits 31:16 = Scale factor to apply to this sky source,
         before adding to the other 3 sky sources that contribute to a data stream.
         After scaling by this scale factor and summing with the other 3 sky sources,
         the result is divided by 2^16 = 65536 and rounded and saturated to 8 bit values.
         **Sinusoids**
         Generated in 2.14 format, min and max values are -16384 to 16384
         Standard deviation is 16384/sqrt(2) = 11585
         A scale factor of 1 gives a sinusoid with amplitude +/-0.25 in the SPS data.
         (i.e. non-existent, unless dithered by also adding noise)
         A scale factor of 512 gives a sinusoid with amplitude +/-128, which may result in clipping.
         **Pseudo-random Noise**
         Approximately Gaussian distributed, standard deviation = 209.03, range -1020 to 1020
         A scale factor of 65536/209.03 ~ 314 results in noise with standard deviation of 1 in SPS data
         A scale factor of  20*65536/209.03 ~ 6270 results in noise with standard deviation = 20
         and max value of (6270/65536) * 1020 = 97
       bits 63:31 = Unused, could be used for pulsar-like windowing of noise ?
"""

import argparse
import typing

import numpy as np
import yaml


def command_line_args():
    parser = argparse.ArgumentParser(
        description="CNIC datagen module configuration generator"
    )
    parser.add_argument(
        "-d",
        "--data",
        type=argparse.FileType(mode="wt"),
        help="File to write configuration data to",
        required=False,
    )
    parser.add_argument(
        "-H0",
        "--HBM0",
        help="HBM buffer 0 data from firmware to check",
        type=argparse.FileType(mode="r"),
    )
    parser.add_argument(
        "-H1",
        "--HBM1",
        help="HBM buffer 1 data from firmware to check",
        type=argparse.FileType(mode="r"),
    )
    parser.add_argument(
        "-f",
        "--filter",
        help="Interpolation filter taps",
        type=argparse.FileType(mode="r"),
    )
    parser.add_argument(
        "configuration",
        help="Test Configuration (YAML)",
        type=argparse.FileType(mode="r"),
    )
    return parser.parse_args()


def parse_config(file: typing.Union[typing.IO, str]) -> typing.Dict:
    """
    Reads configuration YAML file, checks if required values are present.

    :param file: YAML file object or string
    :return: Settings dict, guaranteed to contain the keys specified in required_keys
    :raises AssertionError: if required key(s) are missing
    """
    config = yaml.safe_load(file)

    required_keys = {"sources"}
    assert config.keys() >= required_keys, "Configuration YAML missing required key(s)"
    print("\n⚙️\tSettings:")
    for parameter, value in config.items():
        if parameter == "sources":
            print("\tsources:")
            for n, src_cfg in config["sources"].items():
                print(f"\t  {n}:")
                for src_param, src_val in src_cfg.items():
                    print(f"\t    {src_param}: {src_val}")
        else:
            print(f"\t{parameter}: {value}")
    return config


def datagen_config(config):
    """
    :param config: configuration data as read from the yaml file,
    config["sources"][source number]["poly","sky_freq","seed","tone_freq","select_tone","scale"]
    :return: numpy array of uint32 to be loaded into the cnic datagen module.
    """
    # source keys start from 0, so add 1 to get total sources
    total_sources = max(list(config["sources"].keys())) + 1
    print(f"total_sources = {total_sources}")
    # each source has 64 bytes of configuration data
    # store as 4-byte integers so 16 entries per source
    config_array = np.zeros(total_sources * 16, np.uint32)
    poly_coefficients = np.zeros(6, np.float64)
    for n, src_cfg in config["sources"].items():
        for c_index in range(6):
            poly_coefficients[c_index] = np.float64(src_cfg["poly"][c_index])
        config_array[(n * 16) : (n * 16 + 12)] = np.frombuffer(
            poly_coefficients.tobytes(), dtype=np.uint32
        )
        config_array[(n * 16 + 12) : (n * 16 + 14)] = np.frombuffer(
            np.float64(src_cfg["sky_freq"]).tobytes(), dtype=np.uint32
        )
        if src_cfg["select_tone"]:
            if src_cfg["tone_freq"] < 0:
                # high bit is already set.
                low16bits = np.uint32(65536 + src_cfg["tone_freq"])
            else:  # set high bit
                low16bits = np.uint32(32768 + src_cfg["tone_freq"])
        else:
            low16bits = np.uint32(src_cfg["seed"])
        high16bits = np.uint32(src_cfg["scale"] * 65536)
        config_array[(n * 16 + 14)] = high16bits + low16bits
    return config_array


def rand64(seed, n_samples, gen_real):
    """
    Generate n_samples of random numbers, using the xorshift64 algorithm
    xorshift64 generates 64 bit values by shifting and xoring
    This function generates 12 bit values by chopping the 64-bit value
    into 8 x 8bit values and alternately adding and subtracting.
    c++ version:
    uint64_t xorshift64(uint64_t x){
        x ^= x << 13; x ^= x >> 7; x ^= x << 17;
        return x;
    }

    :param n_samples: Number of samples to generate
    :param seed: 15 bit integer initial value for the random number generator
    seed is expanded to a 64 bit value as per the firmware.
    :param gen_real: if true, creates the 64 bit seed from seed according to the
    way the firmware does for the real value, otherwise for the imaginary value.
    """
    d64 = np.zeros(n_samples, dtype=np.uint64)
    dout = np.zeros(n_samples, dtype=np.int32)
    cur_state = np.uint64(seed)
    if gen_real:
        cur_state = (
            np.left_shift(cur_state, np.uint64(48))
            + np.uint64(0xFFFF000000)
            + np.uint64(cur_state)
        )
    else:
        cur_state = (
            np.left_shift(cur_state, np.uint64(48))
            + np.uint64(0xFF0000FF0000)
            + np.uint64(cur_state)
        )

    for sample in range(n_samples):
        d64[sample] = cur_state
        # split into 8x8 bit values
        s8b = np.int32(np.frombuffer(cur_state.tobytes(), dtype=np.int8))
        # sum as per firmware to get ~12 bit value
        dout[sample] = (
            s8b[0] - s8b[1] + s8b[2] - s8b[3] + s8b[4] - s8b[5] + s8b[6] - s8b[7]
        )
        cur_state = np.bitwise_xor(cur_state, np.left_shift(cur_state, np.uint64(13)))
        cur_state = np.bitwise_xor(cur_state, np.right_shift(cur_state, np.uint64(7)))
        cur_state = np.bitwise_xor(cur_state, np.left_shift(cur_state, np.uint64(17)))
    return dout


def create_stream(config, filters, stream_index, n_samples):
    """
    create a data stream from the config data.
    Uses data specification for 4 sources,
    in config["sources"][stream_index*4:stream_index*4+4]
    :param config: configuration data as read from the yaml file,
    config["sources"][source number]["poly","sky_freq","seed","tone_freq","select_tone","scale"]
    :param filters: 2048x16 numpy array with interpolation filter coefficients.
    :param stream_index: specifies the set of sources to use
    :param n_samples: Number of samples to generate
    """
    # Generate an extra 2048 samples to prevent running out of samples
    # due to the coarse (sample) delay.
    n_samples_extra = n_samples + 2048
    source_samples = np.zeros(n_samples_extra, dtype=np.complex128)
    interpolated = np.zeros(n_samples, dtype=np.complex128)
    rotated = np.zeros(n_samples, dtype=np.complex128)
    scaled = np.zeros(n_samples, dtype=np.complex128)
    summed = np.zeros((n_samples, 2), dtype=np.complex128)
    for pol in range(2):
        for data_source in range(4):
            # 8 consecutive sources specify one data stream.
            # 4 for the first polarisation, then 4 for the second polarisation
            s_index = stream_index * 8 + pol * 4 + data_source
            if s_index in config["sources"]:
                if config["sources"][s_index]["select_tone"]:
                    phase_step = np.double(config["sources"][s_index]["tone_freq"])
                    phase = 2 * np.pi * phase_step / 32768 * np.arange(n_samples_extra)
                    source_samples = 16384 * np.exp(1j * phase)
                else:
                    seed = config["sources"][s_index]["seed"]
                    source_samples = np.double(
                        rand64(seed, n_samples_extra, True)
                    ) + 1j * np.double(rand64(seed, n_samples_extra, False))
                    # src_std = np.std(source_samples)
                # Process blocks of 16 samples
                for sample_block in range(n_samples // 16):
                    poly = config["sources"][s_index]["poly"]
                    t = sample_block * 16 * 1080.0e-9
                    # delay in ns
                    delay = (
                        poly[0]
                        + poly[1] * t
                        + poly[2] * (t**2)
                        + poly[3] * (t**3)
                        + poly[4] * (t**4)
                        + poly[5] * (t**5)
                    )
                    delay_samples = np.uint32(np.floor(delay / 1080.0))
                    interp_filter_select = np.uint32(
                        np.round(2048 * (delay / 1080.0 - delay_samples))
                    )
                    interp_filter = filters[interp_filter_select, :]
                    phase_correction = np.exp(
                        1j * 2 * np.pi * delay * config["sources"][s_index]["sky_freq"]
                    )
                    for sample in range(16):
                        # The sample the first filter tap applies to
                        start_sample = (
                            17 + sample_block * 16 + delay_samples - 15 + sample
                        )
                        # The output sample we are generating
                        cur_sample = sample_block * 16 + sample
                        samples_selected = source_samples[
                            start_sample : (start_sample + 16)
                        ]
                        # scale by 256 to match intermediate value in the firmware
                        interpolated[cur_sample] = (
                            np.sum(interp_filter * samples_selected) / 256
                        )
                        rotated[cur_sample] = (
                            interpolated[cur_sample] * phase_correction
                        )
                        scaled[cur_sample] = (
                            rotated[cur_sample] * config["sources"][s_index]["scale"]
                        )
                summed[:, pol] += scaled
    # scaling factor: 14 bits for the filter taps,
    # 16 bits for the configured scale factor.
    # less 8 bits already scaled at the output of the filter
    summed = np.round(summed / 2**22)
    # summed_std = np.std(summed)
    return summed


def main():
    # Read command-line arguments
    args = command_line_args()
    config = parse_config(args.configuration)
    # Get the interpolation filters, returns a 2048x16 numpy array
    filters = np.genfromtxt(args.filter, dtype=np.int32)
    # convert config into a data file to load into the firmware
    cfg_array = datagen_config(config)
    # Write to file.
    # Writes are in blocks of 16 words, preceded by the address to write to.
    total_blocks = len(cfg_array) // 16
    first_block = True
    for b in range(total_blocks):
        # 64 bytes per block
        block_addr = b * 64
        non_zero = np.any(cfg_array[b * 16 : (b * 16 + 16)])
        if non_zero:
            if not first_block:
                args.data.write("\n")
            first_block = False
            args.data.write(f"[{block_addr:08x}]")
            for n in range(16):
                args.data.write(f"\n{cfg_array[b*16+n]:08x}")

    s0pol0 = create_stream(config, filters, 0, 64)
    for sp in range(5):
        print(
            f"{s0pol0[sp*8+0]}, {s0pol0[sp*8+1]}, {s0pol0[sp*8+2]}, {s0pol0[sp*8+3]}, "
            f"{s0pol0[sp*8+4]}, {s0pol0[sp*8+5]}, {s0pol0[sp*8+6]}, {s0pol0[sp*8+7]}"
        )

    # check against data downloaded from HBM, either from simulation or real data.
    # Maximum 512 kBytes per stream
    # Total samples checked is double this value,
    # since data is taken from both HBM buffers
    samples_to_check = 512  # 2048 = 1 SPS packet
    if args.HBM0:
        # Load data saved from the two HBM buffers
        fw_data_list0 = []
        fw_data_list1 = []
        for hex_word in args.HBM0:
            fw_data_list0.append(int(hex_word, 16))
        for hex_word in args.HBM1:
            fw_data_list1.append(int(hex_word, 16))
        fw_data0_int32 = np.array(fw_data_list0, dtype=np.uint32)
        fw_data0_bytes = 4 * fw_data0_int32.shape[0]
        fw_data1_int32 = np.array(fw_data_list1, dtype=np.uint32)
        # find how many streams were downloaded in the data file
        streams_to_check = fw_data0_bytes // 524288
        fw_data0_int8 = np.frombuffer(fw_data0_int32.tobytes(), dtype=np.int8)
        fw_data1_int8 = np.frombuffer(fw_data1_int32.tobytes(), dtype=np.int8)
        for stream in range(streams_to_check):
            # There are (512 kbytes)/(4 bytes/sample) = 128kSamples in a stream,
            # 2 polarisations
            fw_data = np.zeros((2 * samples_to_check, 2), dtype=np.complex128)
            stream_start = stream * 524288
            # get the two polarisations
            fw_data[0:samples_to_check, 0] = (
                fw_data0_int8[stream_start : (stream_start + samples_to_check * 4) : 4]
                + 1j
                * fw_data0_int8[
                    (stream_start + 1) : (stream_start + samples_to_check * 4) : 4
                ]
            )
            fw_data[samples_to_check : (2 * samples_to_check), 0] = (
                fw_data1_int8[stream_start : (stream_start + samples_to_check * 4) : 4]
                + 1j
                * fw_data1_int8[
                    (stream_start + 1) : (stream_start + samples_to_check * 4) : 4
                ]
            )
            fw_data[0:samples_to_check, 1] = (
                fw_data0_int8[
                    stream_start + 2 : (stream_start + samples_to_check * 4) : 4
                ]
                + 1j
                * fw_data0_int8[
                    (stream_start + 3) : (stream_start + samples_to_check * 4) : 4
                ]
            )
            fw_data[samples_to_check : (2 * samples_to_check), 1] = (
                fw_data1_int8[
                    stream_start + 2 : (stream_start + samples_to_check * 4) : 4
                ]
                + 1j
                * fw_data1_int8[
                    (stream_start + 3) : (stream_start + samples_to_check * 4) : 4
                ]
            )
            # Compare with the python generated version
            check_data = create_stream(config, filters, stream, 2 * samples_to_check)
            check_diff = (
                fw_data[0 : 2 * samples_to_check, :]
                - check_data[0 : 2 * samples_to_check, :]
            )
            max_diff_pol0re = np.max(np.real(check_diff[:, 0]))
            max_diff_pol0im = np.max(np.imag(check_diff[:, 0]))
            max_diff_pol1re = np.max(np.real(check_diff[:, 1]))
            max_diff_pol1im = np.max(np.imag(check_diff[:, 1]))
            print(
                f"Stream {stream} max diff : pol 0 re = {max_diff_pol0re}, "
                f"pol 0 im = {max_diff_pol0im}, pol 1 re = {max_diff_pol1re}, "
                f"pol 1 im = {max_diff_pol1im}"
            )


if __name__ == "__main__":
    main()
