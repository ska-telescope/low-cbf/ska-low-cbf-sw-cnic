# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

"""Define module interfaces for downstream use."""

from .icl.cnic_fpga import CnicFpga
from .icl.hbm_packet_controller import HbmPacketController
from .icl.ptp import Ptp
from .icl.ptp_scheduler import TIME_STR_FORMAT, PtpScheduler
from .icl.spead_sps import SpeadSPS
from .icl.system import System, mask_shift
from .icl.vd_config import StreamConfig, frequency_from_id, id_from_frequency
from .icl.vd_datagen import (
    DELAY_POLY_COEFFS,
    NULL_SOURCE,
    SOURCES_PER_STREAM,
    DataGenerator,
    Source,
    pad_source_list,
)
from .icl.virtual_digitiser import VDChannelConfig, VirtualDigitiser
