# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Test the VD Data Generator peripheral ICL"""
import json

import numpy as np
import pytest
from ska_low_cbf_fpga.args_fpga import ArgsWordType

from ska_low_cbf_sw_cnic.icl.vd_datagen import (
    SOURCE_CONFIG_WORDS,
    SOURCES_PER_STREAM,
    Source,
    pad_source_list,
)


def test_pad_source_list():
    """Ensure list is padded to the right length."""
    sources = [Source(tone=False, channel_frequency=0.1, scale=0)] * 3
    assert len(sources) != SOURCES_PER_STREAM
    assert len(pad_source_list(sources, SOURCES_PER_STREAM)) == SOURCES_PER_STREAM


TEST_SOURCES = [
    Source(
        tone=False,
        channel_frequency=0.1,
        scale=222,
        seed=456,
        delay_polynomial=np.array([1, 2, 3, 4, 5, 6], dtype=np.float64),
    ),
    Source(
        tone=True,
        channel_frequency=0.1,
        scale=-123,
        fine_frequency=-456,
        delay_polynomial=np.array([1, 2, 3, 4, 5, 6], dtype=np.float64),
    ),
]


@pytest.mark.parametrize("source", TEST_SOURCES)
def test_source_register_conversion(source):
    """This doesn't guarantee it's right, but at least it's symmetric..."""
    assert source.from_registers(source.as_registers()) == source


def test_source_to_json():
    """Ensure we can serialise, as used in DataGenerator.configure property."""
    json.dumps(TEST_SOURCES, default=dict)
    # loading from registers is where the real problem was
    json.dumps(
        Source.from_registers(np.zeros(SOURCE_CONFIG_WORDS, dtype=ArgsWordType)),
        default=dict,
    )
