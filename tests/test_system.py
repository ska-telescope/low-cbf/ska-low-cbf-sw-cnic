# -*- coding: utf-8 -*-
#
# Copyright (c) 2024 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""
Tests associated with CNIC "System" peripheral abstraction.
"""
import pytest

from ska_low_cbf_sw_cnic import mask_shift


@pytest.mark.parametrize(
    "value, mask, expected",
    (
        (0b1001_0110, 0b1111_0000, 0b1001),
        (0b1001_0110, 0b0000_1111, 0b0110),
    ),
)
def test_mask_shift(value, mask, expected):
    assert mask_shift(value, mask) == expected
