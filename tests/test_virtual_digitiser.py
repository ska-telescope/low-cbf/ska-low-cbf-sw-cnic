# -*- coding: utf-8 -*-
#
# Copyright (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Test the Virtual Digitiser peripheral ICL"""
import json
from random import randint

import numpy as np
import pytest
from ska_low_cbf_fpga import ArgsMap, ArgsSimulator
from ska_low_cbf_fpga.args_fpga import ArgsWordType

from ska_low_cbf_sw_cnic.icl.virtual_digitiser import (
    SpeadVersion,
    VDChannelConfig,
    VirtualDigitiser,
    _split_into_two,
    _time_between_streams,
)

from .fpgamap_24022410 import FPGAMAP


@pytest.fixture
def vd():
    """
    Create a simulated peripheral instance
    """
    return VirtualDigitiser(ArgsSimulator(fpga_map=FPGAMAP), ArgsMap(FPGAMAP)["vd"])


def test_split_into_two():
    """Test register split function"""
    assert (0xAB, 0xCD) == _split_into_two(0xAB_0000_00CD)


def test_vd_chan_cfg():
    """Test VDChannelConfig creation & flattening"""
    np.testing.assert_equal(
        np.array([1, 2, 3, 4, 5, 6, 0], dtype=ArgsWordType),
        VDChannelConfig(
            scan=1, beam=2, frequency=3, substation=4, subarray=5, station=6
        ).as_registers(),
    )


@pytest.mark.parametrize(
    "n_streams, time_ns", [(1, 505), (576, 505), (1728, 100), (2048, 0)]
)
def test_time_between_streams(n_streams, time_ns):
    """Verify time delay equation."""
    assert _time_between_streams(n_streams) == time_ns


chan_cfg = [
    VDChannelConfig(
        scan=1234,
        beam=1,
        frequency=freq,
        substation=1,
        subarray=1,
        station=stn,
    )
    for freq in range(100, 196)
    for stn in [345, 350, 352, 355, 431, 434]
]

scan_id_low = 7

v3_past_32bit = (0x1_0000_0000, 0xFF_0000_0000)
"""Scan ID high component that is larger than 32 bits (SPEAD v3)"""

stream_cfg = [
    {
        "scan": scan_id_low + scan_id_high,
        "subarray": 1,
        "station": 340,
        "substation": 1,
        "frequency": 100,
        "beam": 1,
    }
    for scan_id_high in v3_past_32bit
]

v3_too_big = 0xFFFF_FFFF_0000_0001
"""Scan ID that is larger than 48 bits (SPEAD v3)"""

stream_big_cfg = [
    {
        "scan": v3_too_big,
        "subarray": 1,
        "station": 340,
        "substation": 1,
        "frequency": 100,
        "beam": 1,
    }
]


class TestVirtualDigitiser:
    def test_config_channels(self, vd):
        """Ensure we can write a large block of channel configs"""

        vd.configure_channels(chan_cfg)
        assert vd.number_of_valid_lines_in_vd_ram == 575
        assert vd.data[0] == 1234  # scan in word 0
        assert vd.data[5] == 345  # station in word 5
        assert vd.data[575 * 8] == 1234
        assert vd.data[5 + 575 * 8] == 434
        # 576*8 is past the end of our config so should not be written
        assert vd.data[576 * 8] == 0

    # Note: not bothering to test time interface as it will change "soon"

    def test_pulsar_enable_control(self, vd):
        """Basic pulsar mode enable/disable test"""
        for enable in (True, False, True, False, False, True, False):
            vd.configure_pulsar_mode(enable)
            assert vd.enable_pulsar_timing == int(enable)

    def test_pulsar_sample_count(self, vd):
        """Check registers are written correctly and stay unchanged when
        the mode is enabled/disabled
        """
        MIN_SAMPLE_COUNT = 16
        MAX_SAMPLE_COUNT = 0xFFFF_FFFF  # 32-bit FPGA registers

        for _ in range(10):
            start = randint(MIN_SAMPLE_COUNT, MAX_SAMPLE_COUNT)
            on = randint(MIN_SAMPLE_COUNT, MAX_SAMPLE_COUNT)
            off = randint(MIN_SAMPLE_COUNT, MAX_SAMPLE_COUNT)
            vd.configure_pulsar_mode(True, (start, on, off))

            assert vd.pulsar_start_sample_count == start
            assert vd.pulsar_on_sample_count == on
            assert vd.pulsar_off_sample_count == off

            # check the registers are unchanged after toggling the mode
            for enable in (False, True):
                vd.configure_pulsar_mode(enable)
                assert vd.pulsar_start_sample_count == start
                assert vd.pulsar_on_sample_count == on
                assert vd.pulsar_off_sample_count == off

    def test_config_readout(self, vd):
        vd.configure_channels(chan_cfg)
        str_cfg = [dict(cfg) for cfg in chan_cfg]
        assert json.loads(vd.configuration.value) == str_cfg

    def test_speadv2_large_scan_id(self, vd):
        """Confirm scan ID high portion is ignored for SPEAD v2"""
        vd.sps_packet_version = SpeadVersion.v2
        assert vd.sps_packet_version.value == SpeadVersion.v2
        obj_array = [VDChannelConfig(**config) for config in stream_cfg]
        vd.configure_channels(obj_array)
        for cfg in json.loads(vd.configuration.value):
            assert cfg["scan"] == scan_id_low

    def test_speadv3_large_scan_id(self, vd):
        """Confirm scan ID high portion is fine for SPEAD v3"""
        vd.sps_packet_version = SpeadVersion.v3
        assert vd.sps_packet_version.value == SpeadVersion.v3
        obj_array = [VDChannelConfig(**_) for _ in stream_cfg]
        vd.configure_channels(obj_array)
        for i, cfg in enumerate(json.loads(vd.configuration.value)):
            assert cfg["scan"] == v3_past_32bit[i] + scan_id_low

    def test_speadv3_ginormous_scan_id(self, vd):
        """Confirm scan ID bits higher than 48 are stripped for SPEAD v3"""
        LOWER_48_BITS = 0x0_FFFF_FFFF_FFFF
        vd.sps_packet_version = SpeadVersion.v3
        assert vd.sps_packet_version.value == SpeadVersion.v3
        vd.configure_channels([VDChannelConfig(**_) for _ in stream_big_cfg])
        for cfg in json.loads(vd.configuration.value):
            assert cfg["scan"] == v3_too_big & LOWER_48_BITS

    def test_configure_time(self, vd):
        """Check time setting (packet counter registers) for SPS SPEAD v3."""
        vd.packet_counter_u = 0  # clear high word
        # set a time that fits within 32 bits - 100 days after epoch
        vd.configure_time(109 * 86400)
        assert vd.packet_counter_u.value == 0
        # set a time that uses 33 bits - 110 days after epoch
        vd.configure_time(110 * 86400)
        assert vd.packet_counter_u.value == 1
        with pytest.raises(Exception):
            vd.configure_time(1000 * 365 * 86400)  # ~1000 years, too big for 40 bits
